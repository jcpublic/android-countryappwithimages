package com.example.countriesapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CountryListViewAdapter extends ArrayAdapter<Country> {
    private ArrayList<Country> countries;
    private Context context;
    private int resource;

    public CountryListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Country> countriesList) {
        super(context, resource, countriesList);
        this.countries = countriesList;     // set the data source
        this.context=context;
        this.resource=resource;             // set the row layout file

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Get the data from the array
        String countryName = this.countries.get(position).getName();
        long population = this.countries.get(position).getPopulation();


        // Connect the row layout file to this adapter
        LayoutInflater inflater = LayoutInflater.from(this.context);
        convertView = inflater.inflate(this.resource,parent,false);


        // Get the textviews and images from the row layout file

        TextView countryLabel = (TextView) convertView.findViewById(R.id.tvCountryName);
        TextView populationLabel = (TextView) convertView.findViewById(R.id.tvPopulation);

        //set text for each text views
        countryLabel.setText(countryName);
        populationLabel.setText("Population: " + population);


        // set images
        ImageView flag = (ImageView) convertView.findViewById(R.id.ivFlag);

        if (countryName.contentEquals("Canada")) {
            flag.setImageResource(R.drawable.canada);
        }
        else if (countryName.contentEquals("Mexico")) {
            flag.setImageResource(R.drawable.mexico);
        }
        else if (countryName.contentEquals("Honduras")) {
            flag.setImageResource(R.drawable.honduras);
        }
        else if (countryName.contentEquals("United States")) {
            flag.setImageResource(R.drawable.united_states);
        }

        return convertView;


    }


}
