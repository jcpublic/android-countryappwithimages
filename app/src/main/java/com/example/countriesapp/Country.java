package com.example.countriesapp;


public class Country implements Comparable {
    private String name;
    private long population;
    private String capital;

    public Country(String n, long p, String capital) {
        this.name = n;
        this.population = p;
        this.capital = capital;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    // comparable function
    // Reference:
    // https://beginnersbook.com/2013/12/java-arraylist-of-object-sort-example-comparable-and-comparator/
    @Override
    public int compareTo(Object c2) {

        // get population of second country
        long c2Population = ((Country)c2).getPopulation();

        // compare current object's population with population of another country
        if (this.getPopulation() == c2Population) {
            return 0;
        }
        else if (this.getPopulation() > c2Population) {
            return 1;
        }
        else {
            return -1;
        }
    }
}
