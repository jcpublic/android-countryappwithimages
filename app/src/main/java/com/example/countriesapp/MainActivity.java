package com.example.countriesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    // initialize your data source
    ArrayList<Country> countries = new ArrayList<Country>();

    // ListView variable


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. add countries to the arraylist
        populateDataSource();

        // 1a. sort the countries by population
        // Reference: https://beginnersbook.com/2013/12/java-arraylist-of-object-sort-example-comparable-and-comparator/
        Collections.sort(countries);


        // 2. Add countries to list view
        CountryListViewAdapter adapter =
                new CountryListViewAdapter(
                        this,
                        R.layout.county_row_layout,
                        countries);

        final ListView listview = (ListView) findViewById(R.id.listview_countries);

        listview.setAdapter(adapter);


        // 3. Make a click handler for list view
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Country c = (Country) listview.getItemAtPosition(position);
                Toast t = Toast.makeText(getApplicationContext(), c.getName(), Toast.LENGTH_SHORT);
                t.show();
            }
        });

    }

    // This function adds countries to the country array
    public void populateDataSource() {
        Country c1 = new Country("United States",327929000,"Washington, DC");
        Country c2 = new Country("Mexico",132328789,"Mexico City");
        Country c3 = new Country("Canada",37279800,"Ottawa");
        Country c4 = new Country("Honduras",9012229,"Tegucigalpa");

        countries.add(c1);
        countries.add(c2);
        countries.add(c3);
        countries.add(c4);
    }

}
